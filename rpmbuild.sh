#!/bin/sh
set -e
if [[ ! -z "$DEBUG" ]]; then
   set -x
fi

if [ -z "$SPEC" ]; then
   if [ "`find . -maxdepth 1 -name '*.spec'| wc -l`" -eq 1 ]; then
       SPEC=`find . -maxdepth 1 -name '*.spec' |head -n 1`
   else
       exit 1
   fi
fi

if [ -z "$LOCALSOURCEDIR" ]; then
    LOCALSOURCEDIR="$(dirname $(readlink -f $SPEC))"
fi

if [ -z "$RPMBUILDROOT" ]; then
    RPMBUILDROOT="$LOCALSOURCEDIR/rpmbuild"
fi

prepare() {
    if [ ! -d "$RPMBUILDROOT/{BUILD,RPMS,SOURCES,SRPMS}" ]; then
        sudo mkdir -p $RPMBUILDROOT/{BUILD,RPMS,SOURCES,SRPMS}
        sudo chown $USER -R $RPMBUILDROOT
    fi
}

get_deps() {
    find "$LOCALSOURCEDIR" -maxdepth 1 -type f -exec cp {} $RPMBUILDROOT/SOURCES/ \; -print

    if egrep -q '^Source0:.*' "$SPEC"; then
        spectool -g "$SPEC" --directory "$RPMBUILDROOT/SOURCES/"
    fi

    if [ `id -u` != 0 ]; then
        sudo yum-builddep $DNF_OPT --setopt=cachedir=$RPMBUILDROOT/cache -y $SPEC
    else
        yum-builddep $DNF_OPT --setopt=cachedir=$RPMBUILDROOT/cache -y $SPEC
    fi
}

build() {
    rpmbuild --define "_topdir $RPMBUILDROOT" --define "_tmppath $RPMBUILDROOT/tmp" -bb $SPEC
}

main() {
    prepare
    get_deps
    build
}

main

