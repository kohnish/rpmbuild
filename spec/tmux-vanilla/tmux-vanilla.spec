Name:           tmux-vanilla
Version:        3.4
Release:        0%{?dist}
Summary:        IPSec utility
License:        GPLv2 and MIT
URL:            http://github.com/tmux
Source0:        https://github.com/tmux/tmux/archive/refs/heads/master.zip
BuildRequires:  gcc ncurses-devel libevent-devel automake byacc
Conflicts:      tmux

%description
tmux

%prep
%setup -q -n tmux-master

%build
./autogen.sh
%configure --prefix=/usr
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
/usr/bin/tmux
/usr/share/man/man1/tmux.1.gz

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
