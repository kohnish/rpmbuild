Name: kernel-stable
Summary: The Linux Kernel
Version: 5.19.0
Release: 11
%define major_version 5
%define original_version 5.19
%define modules_version 5.19.0
License: GPL
Group: System Environment/Kernel
Vendor: The Linux Community
URL: http://www.kernel.org
Source0: https://cdn.kernel.org/pub/linux/kernel/v%{major_version}.x/linux-%{original_version}.tar.xz
#Source0: https://git.kernel.org/torvalds/t/linux-%{original_version}.tar.gz
Source1: config
BuildRequires: flex bison openssl openssl-devel elfutils-libelf-devel bc rsync gcc make perl hostname dwarves zstd
%define __spec_install_post /usr/lib/rpm/brp-compress || :
%define debug_package %{nil}
%define _build_id_links none


%description
The Linux Kernel, the operating system core itself

%package headers
Summary: Header files for the Linux kernel for use by glibc
Group: Development/System
Provides: kernel-headers
Conflicts: kernel-headers
%description headers
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%package devel
Summary: Kernel source files
Group: Development/System
%description devel
Kernel-devel includes all source codes and generated configurations.

%prep
# %%setup -q -n linux-%{version}
%setup -q -n linux-%{original_version}

%build
#sed -i 's/-rc2//g' Makefile
cp %{SOURCE1} ./.config
#patch -p1 < /home/kohei/rtw88_debug.patch
make bzImage %{?_smp_mflags}
make modules %{?_smp_mflags} 

%install
mkdir -p %{buildroot}/usr/lib/modules/%{modules_version}
make %{?_smp_mflags} INSTALL_PATH=%{buildroot}/usr/lib/modules/%{modules_version} install
make %{?_smp_mflags} INSTALL_MOD_PATH=%{buildroot}/usr modules_install
make %{?_smp_mflags} INSTALL_HDR_PATH=%{buildroot}/usr headers_install
mkdir -p %{buildroot}/usr/src/kernels
rm -rf %{_builddir}/linux-%{original_version}/Documentation
rm -rf %{_builddir}/linux-%{original_version}/MAINTAINERS
rm -rf %{_builddir}/linux-%{original_version}/LICENSES
rm -rf %{_builddir}/linux-%{original_version}/COPYING
rm -rf %{_builddir}/linux-%{original_version}/CREDITS
rm -rf %{_builddir}/linux-%{original_version}/vmlinux
rm -rf %{_builddir}/linux-%{original_version}/drivers
sh -c "cd %{_builddir}/linux-%{original_version}/arch && ls -1|fgrep -v x86 |fgrep -v Kconfig|xargs rm -rf"
rm -rf %{_builddir}/linux-%{original_version}/arch/x86/boot
rm -rf %{_builddir}/linux-%{original_version}/samples
find %{_builddir}/linux-%{original_version} -type f -name '*.o' -delete
find %{_builddir}/linux-%{original_version} -type f -name '*.ko' -delete
find %{_builddir}/linux-%{original_version} -type f -name '*.a' -delete
find %{_builddir}/linux-%{original_version} -type f -name '*.gz' -delete
find %{_builddir}/linux-%{original_version} -type f -name '*.hdrtest' -delete
find %{_builddir}/linux-%{original_version} -type f -name '*.cmd' -delete
find %{_builddir}/linux-%{original_version} -type f -name '*.py' -delete
find %{_builddir}/linux-%{original_version} -type f -name '*.c' -delete
find %{_builddir}/linux-%{original_version} -type f -name 'README' -delete
find %{_builddir}/linux-%{original_version} -type f -name '.gitignore' -delete
find %{_builddir}/linux-%{original_version} -type f -name '*.builtin' -delete
find %{_builddir}/linux-%{original_version} -type f -name '*.modinfo' -delete
rm -f %{buildroot}/usr/lib/modules/%{modules_version}/build
rm -f %{buildroot}/usr/lib/modules/%{modules_version}/source
mv %{_builddir}/linux-%{original_version} %{buildroot}/usr/src/kernels/%{modules_version}
rm -rf %{buildroot}/usr/include/scsi
sh -c "cd %{buildroot}/usr/lib/modules/%{modules_version} && ln -sf ../../../src/kernels/%{modules_version} build"
mkdir -p %{_builddir}/linux-%{original_version}
#mv %{buildroot}/usr/lib/modules/%{modules_version}/* %{buildroot}/usr/lib/modules/%{version}/

%posttrans
kernel-install add %{modules_version} /usr/lib/modules/%{modules_version}/vmlinuz

%preun
kernel-install remove %{modules_version} /usr/lib/modules/%{modules_version}/vmlinuz
touch /usr/lib/modules/%{modules_version}/modules.symbols.bin
touch /usr/lib/modules/%{modules_version}/modules.symbols
touch /usr/lib/modules/%{modules_version}/modules.softdep
touch /usr/lib/modules/%{modules_version}/modules.devname
touch /usr/lib/modules/%{modules_version}/modules.dep.bin
touch /usr/lib/modules/%{modules_version}/modules.dep
touch /usr/lib/modules/%{modules_version}/modules.builtin.bin
touch /usr/lib/modules/%{modules_version}/modules.alias.bin
touch /usr/lib/modules/%{modules_version}/modules.alias

%files
%defattr (-, root, root)
/usr/lib/modules/%{modules_version}

%files headers
%defattr (-, root, root)
/usr/include/*

%files devel
%defattr (-, root, root)
/usr/src/kernels/%{modules_version}
