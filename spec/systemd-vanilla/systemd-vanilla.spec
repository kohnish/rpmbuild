Name:           systemd-vanilla
Version:        250
Release:        0%{?dist}
Summary:        Init
License:        GPLv2 and MIT
URL:            http://github.com/systemd
Source0:        https://github.com/systemd/systemd/archive/refs/tags/v%{version}.tar.gz
BuildRequires:  gcc gnu-efi-devel gmp-devel meson python3 python3-jinja2 gperf libcap-devel libmount-devel

%description
systemd

%prep
%setup -q -n systemd-%{version}

%build
meson setup -Ddbus=false -Defi=true -Dcoredump=false -Doomd=false -Dportabled=false -Dpolkit=false -Dxdg-autostart=false -Dquotacheck=false build/
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
/usr/lib/systemd
/usr/share/doc/systemd
/etc/systemd
/etc/X11/xinit/xinitrc.d/50-systemd-user.sh
/etc/init.d/README
/etc/udev/udev.conf
/etc/xdg/systemd/user
/usr/bin/bootctl
/usr/bin/busctl
/usr/bin/hostnamectl
/usr/bin/journalctl
/usr/bin/kernel-install
/usr/bin/localectl
/usr/bin/loginctl
/usr/bin/machinectl
/usr/bin/networkctl
/usr/bin/resolvectl
/usr/bin/systemctl
/usr/bin/systemd-analyze
/usr/bin/systemd-ask-password
/usr/bin/systemd-cat
/usr/bin/systemd-cgls
/usr/bin/systemd-cgtop
/usr/bin/systemd-creds
/usr/bin/systemd-delta
/usr/bin/systemd-detect-virt
/usr/bin/systemd-dissect
/usr/bin/systemd-escape
/usr/bin/systemd-firstboot
/usr/bin/systemd-hwdb
/usr/bin/systemd-id128
/usr/bin/systemd-inhibit
/usr/bin/systemd-machine-id-setup
/usr/bin/systemd-mount
/usr/bin/systemd-notify
/usr/bin/systemd-nspawn
/usr/bin/systemd-path
/usr/bin/systemd-resolve
/usr/bin/systemd-run
/usr/bin/systemd-socket-activate
/usr/bin/systemd-stdio-bridge
/usr/bin/systemd-sysext
/usr/bin/systemd-sysusers
/usr/bin/systemd-tmpfiles
/usr/bin/systemd-tty-ask-password-agent
/usr/bin/systemd-umount
/usr/bin/timedatectl
/usr/bin/udevadm
/usr/bin/userdbctl
/usr/include/libudev.h
/usr/include/systemd/_sd-common.h
/usr/include/systemd/sd-bus-protocol.h
/usr/include/systemd/sd-bus-vtable.h
/usr/include/systemd/sd-bus.h
/usr/include/systemd/sd-daemon.h
/usr/include/systemd/sd-device.h
/usr/include/systemd/sd-event.h
/usr/include/systemd/sd-hwdb.h
/usr/include/systemd/sd-id128.h
/usr/include/systemd/sd-journal.h
/usr/include/systemd/sd-login.h
/usr/include/systemd/sd-messages.h
/usr/include/systemd/sd-path.h
%exclude /usr/lib/debug/usr/bin/bootctl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/busctl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/hostnamectl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/journalctl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/localectl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/loginctl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/machinectl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/networkctl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/resolvectl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemctl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-analyze-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-ask-password-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-cat-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-cgls-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-cgtop-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-creds-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-delta-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-detect-virt-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-dissect-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-escape-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-firstboot-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-hwdb-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-id128-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-inhibit-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-machine-id-setup-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-mount-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-notify-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-nspawn-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-path-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-run-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-socket-activate-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-stdio-bridge-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-sysext-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-sysusers-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-tmpfiles-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/systemd-tty-ask-password-agent-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/timedatectl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/udevadm-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/bin/userdbctl-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib/udev/ata_id-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib/udev/cdrom_id-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib/udev/dmi_memory_id-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib/udev/fido_id-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib/udev/mtd_probe-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib/udev/scsi_id-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib/udev/v4l_id-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib64/libnss_myhostname.so.2-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib64/libnss_mymachines.so.2-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib64/libnss_resolve.so.2-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib64/libnss_systemd.so.2-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib64/libsystemd.so.0.33.0-250-0.fc35.x86_64.debug
%exclude /usr/lib/debug/usr/lib64/libudev.so.1.7.3-250-0.fc35.x86_64.debug
/usr/lib/environment.d/99-environment.conf
/usr/lib/kernel/install.conf
/usr/lib/kernel/install.d/50-depmod.install
/usr/lib/kernel/install.d/90-loaderentry.install
/usr/lib/modprobe.d/README
/usr/lib/modprobe.d/systemd.conf
/usr/lib/pam.d/systemd-user
/usr/lib/rpm/macros.d/macros.systemd
/usr/lib/sysctl.d/50-default.conf
/usr/lib/sysctl.d/50-pid-max.conf
/usr/lib/sysctl.d/README
/usr/lib/sysusers.d/README
/usr/lib/sysusers.d/basic.conf
/usr/lib/sysusers.d/systemd-journal.conf
/usr/lib/sysusers.d/systemd-network.conf
/usr/lib/sysusers.d/systemd-resolve.conf
/usr/lib/sysusers.d/systemd-timesync.conf
/usr/lib/tmpfiles.d/README
/usr/lib/tmpfiles.d/etc.conf
/usr/lib/tmpfiles.d/home.conf
/usr/lib/tmpfiles.d/journal-nocow.conf
/usr/lib/tmpfiles.d/legacy.conf
/usr/lib/tmpfiles.d/static-nodes-permissions.conf
/usr/lib/tmpfiles.d/systemd-nspawn.conf
/usr/lib/tmpfiles.d/systemd-pstore.conf
/usr/lib/tmpfiles.d/systemd-resolve.conf
/usr/lib/tmpfiles.d/systemd-tmp.conf
/usr/lib/tmpfiles.d/systemd.conf
/usr/lib/tmpfiles.d/tmp.conf
/usr/lib/tmpfiles.d/var.conf
/usr/lib/tmpfiles.d/x11.conf
/usr/lib/udev/ata_id
/usr/lib/udev/cdrom_id
/usr/lib/udev/dmi_memory_id
/usr/lib/udev/fido_id
/usr/lib/udev/hwdb.d/20-OUI.hwdb
/usr/lib/udev/hwdb.d/20-acpi-vendor.hwdb
/usr/lib/udev/hwdb.d/20-bluetooth-vendor-product.hwdb
/usr/lib/udev/hwdb.d/20-dmi-id.hwdb
/usr/lib/udev/hwdb.d/20-net-ifname.hwdb
/usr/lib/udev/hwdb.d/20-pci-classes.hwdb
/usr/lib/udev/hwdb.d/20-pci-vendor-model.hwdb
/usr/lib/udev/hwdb.d/20-sdio-classes.hwdb
/usr/lib/udev/hwdb.d/20-sdio-vendor-model.hwdb
/usr/lib/udev/hwdb.d/20-usb-classes.hwdb
/usr/lib/udev/hwdb.d/20-usb-vendor-model.hwdb
/usr/lib/udev/hwdb.d/20-vmbus-class.hwdb
/usr/lib/udev/hwdb.d/60-autosuspend-chromiumos.hwdb
/usr/lib/udev/hwdb.d/60-autosuspend-fingerprint-reader.hwdb
/usr/lib/udev/hwdb.d/60-autosuspend.hwdb
/usr/lib/udev/hwdb.d/60-evdev.hwdb
/usr/lib/udev/hwdb.d/60-input-id.hwdb
/usr/lib/udev/hwdb.d/60-keyboard.hwdb
/usr/lib/udev/hwdb.d/60-seat.hwdb
/usr/lib/udev/hwdb.d/60-sensor.hwdb
/usr/lib/udev/hwdb.d/70-analyzers.hwdb
/usr/lib/udev/hwdb.d/70-cameras.hwdb
/usr/lib/udev/hwdb.d/70-joystick.hwdb
/usr/lib/udev/hwdb.d/70-mouse.hwdb
/usr/lib/udev/hwdb.d/70-pointingstick.hwdb
/usr/lib/udev/hwdb.d/70-touchpad.hwdb
/usr/lib/udev/hwdb.d/80-ieee1394-unit-function.hwdb
/usr/lib/udev/hwdb.d/README
/usr/lib/udev/mtd_probe
/usr/lib/udev/rules.d/50-udev-default.rules
/usr/lib/udev/rules.d/60-autosuspend.rules
/usr/lib/udev/rules.d/60-block.rules
/usr/lib/udev/rules.d/60-cdrom_id.rules
/usr/lib/udev/rules.d/60-drm.rules
/usr/lib/udev/rules.d/60-evdev.rules
/usr/lib/udev/rules.d/60-fido-id.rules
/usr/lib/udev/rules.d/60-input-id.rules
/usr/lib/udev/rules.d/60-persistent-alsa.rules
/usr/lib/udev/rules.d/60-persistent-input.rules
/usr/lib/udev/rules.d/60-persistent-storage-tape.rules
/usr/lib/udev/rules.d/60-persistent-storage.rules
/usr/lib/udev/rules.d/60-persistent-v4l.rules
/usr/lib/udev/rules.d/60-sensor.rules
/usr/lib/udev/rules.d/60-serial.rules
/usr/lib/udev/rules.d/64-btrfs.rules
/usr/lib/udev/rules.d/70-camera.rules
/usr/lib/udev/rules.d/70-joystick.rules
/usr/lib/udev/rules.d/70-memory.rules
/usr/lib/udev/rules.d/70-mouse.rules
/usr/lib/udev/rules.d/70-power-switch.rules
/usr/lib/udev/rules.d/70-touchpad.rules
/usr/lib/udev/rules.d/71-seat.rules
/usr/lib/udev/rules.d/73-seat-late.rules
/usr/lib/udev/rules.d/75-net-description.rules
/usr/lib/udev/rules.d/75-probe_mtd.rules
/usr/lib/udev/rules.d/78-sound-card.rules
/usr/lib/udev/rules.d/80-net-setup-link.rules
/usr/lib/udev/rules.d/81-net-dhcp.rules
/usr/lib/udev/rules.d/90-vconsole.rules
/usr/lib/udev/rules.d/99-systemd.rules
/usr/lib/udev/rules.d/README
/usr/lib/udev/scsi_id
/usr/lib/udev/v4l_id
/usr/lib64/libnss_myhostname.so.2
/usr/lib64/libnss_mymachines.so.2
/usr/lib64/libnss_resolve.so.2
/usr/lib64/libnss_systemd.so.2
/usr/lib64/libsystemd.so
/usr/lib64/libsystemd.so.0
/usr/lib64/libsystemd.so.0.33.0
/usr/lib64/libudev.so
/usr/lib64/libudev.so.1
/usr/lib64/libudev.so.1.7.3
/usr/lib64/pkgconfig/libsystemd.pc
/usr/lib64/pkgconfig/libudev.pc
/usr/sbin/halt
/usr/sbin/init
/usr/sbin/poweroff
/usr/sbin/reboot
/usr/sbin/resolvconf
/usr/sbin/runlevel
/usr/sbin/shutdown
/usr/sbin/telinit
/usr/share/bash-completion/completions/bootctl
/usr/share/bash-completion/completions/busctl
/usr/share/bash-completion/completions/hostnamectl
/usr/share/bash-completion/completions/journalctl
/usr/share/bash-completion/completions/kernel-install
/usr/share/bash-completion/completions/localectl
/usr/share/bash-completion/completions/loginctl
/usr/share/bash-completion/completions/machinectl
/usr/share/bash-completion/completions/networkctl
/usr/share/bash-completion/completions/resolvectl
/usr/share/bash-completion/completions/systemctl
/usr/share/bash-completion/completions/systemd-analyze
/usr/share/bash-completion/completions/systemd-cat
/usr/share/bash-completion/completions/systemd-cgls
/usr/share/bash-completion/completions/systemd-cgtop
/usr/share/bash-completion/completions/systemd-delta
/usr/share/bash-completion/completions/systemd-detect-virt
/usr/share/bash-completion/completions/systemd-id128
/usr/share/bash-completion/completions/systemd-nspawn
/usr/share/bash-completion/completions/systemd-path
/usr/share/bash-completion/completions/systemd-resolve
/usr/share/bash-completion/completions/systemd-run
/usr/share/bash-completion/completions/timedatectl
/usr/share/bash-completion/completions/udevadm
/usr/share/dbus-1/interfaces/org.freedesktop.LogControl1.xml
/usr/share/dbus-1/interfaces/org.freedesktop.hostname1.xml
/usr/share/dbus-1/interfaces/org.freedesktop.locale1.xml
/usr/share/dbus-1/interfaces/org.freedesktop.login1.Manager.xml
/usr/share/dbus-1/interfaces/org.freedesktop.login1.Seat.xml
/usr/share/dbus-1/interfaces/org.freedesktop.login1.Session.xml
/usr/share/dbus-1/interfaces/org.freedesktop.login1.User.xml
/usr/share/dbus-1/interfaces/org.freedesktop.machine1.Image.xml
/usr/share/dbus-1/interfaces/org.freedesktop.machine1.Machine.xml
/usr/share/dbus-1/interfaces/org.freedesktop.machine1.Manager.xml
/usr/share/dbus-1/interfaces/org.freedesktop.network1.DHCPServer.xml
/usr/share/dbus-1/interfaces/org.freedesktop.network1.Link.xml
/usr/share/dbus-1/interfaces/org.freedesktop.network1.Manager.xml
/usr/share/dbus-1/interfaces/org.freedesktop.network1.Network.xml
/usr/share/dbus-1/interfaces/org.freedesktop.resolve1.DnssdService.xml
/usr/share/dbus-1/interfaces/org.freedesktop.resolve1.Link.xml
/usr/share/dbus-1/interfaces/org.freedesktop.resolve1.Manager.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Automount.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Device.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Job.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Manager.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Mount.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Path.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Scope.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Service.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Slice.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Socket.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Swap.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Target.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Timer.xml
/usr/share/dbus-1/interfaces/org.freedesktop.systemd1.Unit.xml
/usr/share/dbus-1/interfaces/org.freedesktop.timedate1.xml
/usr/share/dbus-1/services/org.freedesktop.systemd1.service
/usr/share/dbus-1/system-services/org.freedesktop.hostname1.service
/usr/share/dbus-1/system-services/org.freedesktop.locale1.service
/usr/share/dbus-1/system-services/org.freedesktop.login1.service
/usr/share/dbus-1/system-services/org.freedesktop.machine1.service
/usr/share/dbus-1/system-services/org.freedesktop.network1.service
/usr/share/dbus-1/system-services/org.freedesktop.resolve1.service
/usr/share/dbus-1/system-services/org.freedesktop.systemd1.service
/usr/share/dbus-1/system-services/org.freedesktop.timedate1.service
/usr/share/dbus-1/system-services/org.freedesktop.timesync1.service
/usr/share/dbus-1/system.d/org.freedesktop.hostname1.conf
/usr/share/dbus-1/system.d/org.freedesktop.locale1.conf
/usr/share/dbus-1/system.d/org.freedesktop.login1.conf
/usr/share/dbus-1/system.d/org.freedesktop.machine1.conf
/usr/share/dbus-1/system.d/org.freedesktop.network1.conf
/usr/share/dbus-1/system.d/org.freedesktop.resolve1.conf
/usr/share/dbus-1/system.d/org.freedesktop.systemd1.conf
/usr/share/dbus-1/system.d/org.freedesktop.timedate1.conf
/usr/share/dbus-1/system.d/org.freedesktop.timesync1.conf
/usr/share/factory/etc/issue
/usr/share/factory/etc/nsswitch.conf
/usr/share/factory/etc/pam.d/other
/usr/share/factory/etc/pam.d/system-auth
/usr/share/pkgconfig/systemd.pc
/usr/share/pkgconfig/udev.pc
/usr/share/polkit-1/actions/org.freedesktop.hostname1.policy
/usr/share/polkit-1/actions/org.freedesktop.locale1.policy
/usr/share/polkit-1/actions/org.freedesktop.login1.policy
/usr/share/polkit-1/actions/org.freedesktop.machine1.policy
/usr/share/polkit-1/actions/org.freedesktop.network1.policy
/usr/share/polkit-1/actions/org.freedesktop.resolve1.policy
/usr/share/polkit-1/actions/org.freedesktop.systemd1.policy
/usr/share/polkit-1/actions/org.freedesktop.timedate1.policy
/usr/share/systemd/kbd-model-map
/usr/share/systemd/language-fallback-map
/usr/share/zsh/site-functions/_bootctl
/usr/share/zsh/site-functions/_busctl
/usr/share/zsh/site-functions/_hostnamectl
/usr/share/zsh/site-functions/_journalctl
/usr/share/zsh/site-functions/_kernel-install
/usr/share/zsh/site-functions/_localectl
/usr/share/zsh/site-functions/_loginctl
/usr/share/zsh/site-functions/_machinectl
/usr/share/zsh/site-functions/_networkctl
/usr/share/zsh/site-functions/_resolvectl
/usr/share/zsh/site-functions/_sd_hosts_or_user_at_host
/usr/share/zsh/site-functions/_sd_machines
/usr/share/zsh/site-functions/_sd_outputmodes
/usr/share/zsh/site-functions/_sd_unit_files
/usr/share/zsh/site-functions/_systemctl
/usr/share/zsh/site-functions/_systemd
/usr/share/zsh/site-functions/_systemd-analyze
/usr/share/zsh/site-functions/_systemd-delta
/usr/share/zsh/site-functions/_systemd-inhibit
/usr/share/zsh/site-functions/_systemd-nspawn
/usr/share/zsh/site-functions/_systemd-path
/usr/share/zsh/site-functions/_systemd-run
/usr/share/zsh/site-functions/_systemd-tmpfiles
/usr/share/zsh/site-functions/_timedatectl
/usr/share/zsh/site-functions/_udevadm


%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
