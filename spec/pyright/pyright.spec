%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
Name:           pyright
Version:        1.2
Release:        1%{?dist}
Summary:        pyright
License:        GPLv2
URL:            https://gitlab.com/pyright
#Source0:        https://github.com/microsoft/pyright/archive/refs/heads/main.zip
Source0:        https://github.com/smekkley/pyright/archive/refs/heads/master.zip
BuildRequires:  npm

%description
pyright

%prep
%setup -q -n pyright-master

%build
npm install
sh -c 'cd packages/pyright && npm run build'

%install
mkdir -p %{buildroot}/opt/pyright
mkdir -p %{buildroot}/usr/bin/
cp -rp %{_builddir}/pyright-master/packages/pyright/dist/* %{buildroot}/opt/pyright

cat > %{buildroot}/usr/bin/pyright-langserver <<"EOF"
#!/bin/sh
exec node /opt/pyright/pyright-langserver.js "$@"
EOF

cat > %{buildroot}/usr/bin/pyright <<"EOF"
#!/bin/sh
exec node /opt/pyright/pyright.js "$@"
EOF

chmod +x %{buildroot}/usr/bin/pyright-langserver
chmod +x %{buildroot}/usr/bin/pyright

%files
/opt/pyright
/usr/bin/pyright-langserver
/usr/bin/pyright

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

