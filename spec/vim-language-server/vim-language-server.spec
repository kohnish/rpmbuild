%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
Name:           vim-language-server
Version:        0.1
Release:        2%{?dist}
Summary:        vim-language-server
License:        GPLv2
URL:            https://gitlab.com/vim-language-server
Source0:        https://github.com/iamcco/vim-language-server/archive/refs/heads/master.zip
BuildRequires:  npm

%description
vim-language-server

%prep
%setup -q -n vim-language-server-master

%build
npm install
npm run build

%install
mkdir -p %{buildroot}/opt/vim-language-server
mkdir -p %{buildroot}/usr/bin/
cp -rp %{_builddir}/vim-language-server-master/out/* %{buildroot}/opt/vim-language-server/
cd %{buildroot}/usr/bin/

cat > %{buildroot}/usr/bin/vim-language-server <<"EOF"
#!/bin/sh
exec node /opt/vim-language-server/index.js "$@"
EOF

chmod +x %{buildroot}/usr/bin/vim-language-server

%files
/opt/vim-language-server
/usr/bin/vim-language-server

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

