Name:           picom-vanilla
Version:        20.0
Release:        2%{?dist}
Summary:        IPSec utility
License:        GPLv2 and MIT
URL:            http://github.com/picom
Source0:        https://github.com/yshui/picom/archive/refs/heads/next.zip
BuildRequires:  dbus-devel gcc git libconfig-devel libdrm-devel libev-devel libX11-devel libX11-xcb libXext-devel libxcb-devel mesa-libGL-devel meson pcre-devel pixman-devel uthash-devel xcb-util-image-devel xcb-util-renderutil-devel xorg-x11-proto-devel

%description
picom

%prep
%setup -q -n picom-next

%build
meson --prefix=/usr/local . build
ninja -C build
cd build
DESTDIR=%{buildroot} ninja install


%files
/usr/local/bin/picom
/usr/local/bin/picom-trans
%exclude /usr/local/bin/compton
%exclude /usr/local/bin/compton-trans
%exclude /usr/local/share/applications/picom.desktop
%exclude /usr/local/share/icons/hicolor/48x48/apps/compton.png
%exclude /usr/local/share/icons/hicolor/scalable/apps/compton.svg
%exclude /usr/local/share/applications/compton.desktop

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
