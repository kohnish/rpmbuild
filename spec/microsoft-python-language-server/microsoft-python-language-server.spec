%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
%global __os_install_post %{nil}

Name:           microsoft-python-language-server
Version:        0.1
Release:        3%{?dist}
Summary:        IPSec utility
License:        GPLv2 and MIT
URL:            http://github.com/python-language-server
#Source0:        https://github.com/microsoft/python-language-server/archive/refs/heads/master.zip
Source0:        https://github.com/smekkley/python-language-server/archive/refs/heads/master.zip
BuildRequires:  dotnet

%description
microsoft python-language-server

%prep
%setup -q -n python-language-server-master

%build
sh -c 'cd src && dotnet publish -c Release --runtime linux-x64'
mkdir -p %{buildroot}/usr/libexec/python-language-server
mkdir -p %{buildroot}/usr/bin
cp -Rp output/bin/Release/* %{buildroot}/usr/libexec/python-language-server/
cd %{buildroot}/usr/bin
ln -s ../libexec/python-language-server/Microsoft.Python.LanguageServer python-language-server

%files
/usr/bin/python-language-server
/usr/libexec/python-language-server

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
