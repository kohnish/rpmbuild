%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
%global __os_install_post %{nil}
Name:    android-sdk
Version: 30
Release: 1
Group:   Development
License: GPLv2
Summary: %{name}
URL: https://github.com/kohnish/android-ndk
BuildRequires: android-commandlinetools java-1.8.0-openjdk-devel

%description
android sdk and ndk

%install
yes| /opt/android/tools/bin/sdkmanager --sdk_root=%{buildroot}/opt/android/sdk --channel=0 --install 'platforms;android-30' 'build-tools;30.0.2' 'ndk-bundle'

%files
/opt/android/sdk

%changelog
* Tue Dec 10 2019 kohnish <kohnish@gmx.com> - 1.0
- Initial package
