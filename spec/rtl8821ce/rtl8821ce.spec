%global debug_package %{nil}
AutoReqProv: no

Name:    rtl8821ce
Version: 1.5
Release: 7
Group:   System Environment/Kernel
License: GPLv2
Summary: %{name} kernel module(s)
#Source0: https://github.com/kohnish/rtl8821ce/archive/master.zip
Source0: https://github.com/tomaspinho/rtl8821ce/archive/master.zip
#Source0: https://github.com/tomaspinho/rtl8821ce/archive/refs/heads/fix-5.12.zip
URL: https://github.com/kohnish/rtl8821ce

%description
Linux kernel driver for the %{name} chipset for wireless adapter:

%prep
%setup -q -n %{name}-master 
#%setup -q -n %{name}-fix-5.12

%build

%install
sed -i "s/#MODULE_VERSION#/%{version}/g" dkms.conf
mkdir -p %{buildroot}/usr/src/
cp -r %{_builddir}/%{name}-master %{buildroot}/usr/src/%{name}-%{version}
#cp -r %{_builddir}/%{name}-fix-5.12 %{buildroot}/usr/src/%{name}-%{version}

%post
trap '' SIGINT
if [[ $1 -eq 2 ]]; then
    ver=`dkms status rtl8821ce| awk -F',' '{print $2}'`
    if [[ ! -z $ver ]]; then
        dkms remove -m %{name} -v $ver --all || true
    fi
    #find /usr/lib/modules -name 8821ce.ko.xz -delete
fi
dkms add %{name}/%{version} || true
echo "Building and installing"
dkms autoinstall --verbose --kernelver $(uname -r) || true

%preun
trap '' SIGINT
if [[ $1 -eq 0 ]]; then
    echo "Running dkms"
    modprobe -r 8821ce || true
    dkms remove %{name}/%{version} --all || true
    #find /usr/lib/modules -name 8821ce.ko.xz -delete
fi

%clean

%files
/usr/src/%{name}-%{version}

%changelog
* Tue Dec 10 2019 kohnish <kohnish@gmx.com> - 1.0
- Initial package
