%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
%global __os_install_post %{nil}

Name:           gopls
Version:        0.0.1
Release:        2%{?dist}
Summary:        gopls
License:        GPLv2
BuildRequires:  golang git

%description
gopls

%install
export GOPATH=%{_builddir}
go get golang.org/x/tools/gopls@latest
mkdir -p %{buildroot}/usr/bin/
cp %{_builddir}/bin/gopls %{buildroot}/usr/bin/

%files
/usr/bin/gopls

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

