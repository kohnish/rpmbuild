Name: kernel-lts
Summary: The Linux Kernel
Version: 5.16.20
Release: 1
License: GPL
Group: System Environment/Kernel
Vendor: The Linux Community
URL: http://www.kernel.org
Source0: https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-%{version}.tar.xz
Source1: config
BuildRequires: flex bison openssl openssl-devel elfutils-libelf-devel bc rsync gcc make perl hostname dwarves zstd
%define __spec_install_post /usr/lib/rpm/brp-compress || :
%define debug_package %{nil}
%define _build_id_links none

%description
The Linux Kernel, the operating system core itself

%package headers
Summary: Header files for the Linux kernel for use by glibc
Group: Development/System
Provides: kernel-headers
Conflicts: kernel-headers
%description headers
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%package devel
Summary: Kernel source files
Group: Development/System
%description devel
Kernel-devel includes all source codes and generated configurations.

%prep
%setup -q -n linux-%{version}

%build
cp %{SOURCE1} ./.config
make bzImage %{?_smp_mflags}
make modules %{?_smp_mflags} 

%install
mkdir -p %{buildroot}/usr/lib/modules/%{version}
make %{?_smp_mflags} INSTALL_PATH=%{buildroot}/usr/lib/modules/%{version} install
make %{?_smp_mflags} INSTALL_MOD_PATH=%{buildroot}/usr modules_install
make %{?_smp_mflags} INSTALL_HDR_PATH=%{buildroot}/usr headers_install
mkdir -p %{buildroot}/usr/src/kernels
rm -rf %{_builddir}/linux-%{version}/Documentation
rm -rf %{_builddir}/linux-%{version}/MAINTAINERS
rm -rf %{_builddir}/linux-%{version}/LICENSES
rm -rf %{_builddir}/linux-%{version}/COPYING
rm -rf %{_builddir}/linux-%{version}/CREDITS
rm -rf %{_builddir}/linux-%{version}/vmlinux
rm -rf %{_builddir}/linux-%{version}/drivers
sh -c "cd %{_builddir}/linux-%{version}/arch && ls -1|fgrep -v x86 |fgrep -v Kconfig|xargs rm -rf"
rm -rf %{_builddir}/linux-%{version}/arch/x86/boot
rm -rf %{_builddir}/linux-%{version}/samples
find %{_builddir}/linux-%{version} -type f -name '*.o' -delete
find %{_builddir}/linux-%{version} -type f -name '*.ko' -delete
find %{_builddir}/linux-%{version} -type f -name '*.a' -delete
find %{_builddir}/linux-%{version} -type f -name '*.gz' -delete
find %{_builddir}/linux-%{version} -type f -name '*.hdrtest' -delete
find %{_builddir}/linux-%{version} -type f -name '*.cmd' -delete
find %{_builddir}/linux-%{version} -type f -name '*.py' -delete
find %{_builddir}/linux-%{version} -type f -name '*.c' -delete
find %{_builddir}/linux-%{version} -type f -name 'README' -delete
find %{_builddir}/linux-%{version} -type f -name '.gitignore' -delete
find %{_builddir}/linux-%{version} -type f -name '*.builtin' -delete
find %{_builddir}/linux-%{version} -type f -name '*.modinfo' -delete
rm -f %{buildroot}/usr/lib/modules/%{version}/build
rm -f %{buildroot}/usr/lib/modules/%{version}/source
mv %{_builddir}/linux-%{version} %{buildroot}/usr/src/kernels/%{version}
rm -rf %{buildroot}/usr/include/scsi
sh -c "cd %{buildroot}/usr/lib/modules/%{version} && ln -sf ../../../src/kernels/%{version} build"
mkdir -p %{_builddir}/linux-%{version}

%posttrans
kernel-install add %{version} /usr/lib/modules/%{version}/vmlinuz

%preun
kernel-install remove %{version} /usr/lib/modules/%{version}/vmlinuz
touch /usr/lib/modules/%{version}/modules.symbols.bin
touch /usr/lib/modules/%{version}/modules.symbols
touch /usr/lib/modules/%{version}/modules.softdep
touch /usr/lib/modules/%{version}/modules.devname
touch /usr/lib/modules/%{version}/modules.dep.bin
touch /usr/lib/modules/%{version}/modules.dep
touch /usr/lib/modules/%{version}/modules.builtin.bin
touch /usr/lib/modules/%{version}/modules.alias.bin
touch /usr/lib/modules/%{version}/modules.alias

%files
%defattr (-, root, root)
/usr/lib/modules/%{version}

%files headers
%defattr (-, root, root)
/usr/include/*

%files devel
%defattr (-, root, root)
/usr/src/kernels/%{version}
