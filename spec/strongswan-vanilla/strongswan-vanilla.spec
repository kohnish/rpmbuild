Name:           strongswan-vanilla
Version:        5.9.14
Release:        4%{?dist}
Summary:        IPSec utility
License:        GPLv2 and MIT
URL:            http://github.com/strongswan
Source0:        https://github.com/strongswan/strongswan/archive/refs/heads/master.zip
BuildRequires:  gcc
BuildRequires:  openssl-devel
BuildRequires:  gettext-devel
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  libevent
BuildRequires:  m4
BuildRequires:  gperf
BuildRequires:  bison
BuildRequires:  flex
BuildRequires:  gmp-devel
BuildRequires:  systemd-devel
Conflicts:      strongswan libreswan

%global optflags %{optflags} -Wno-format-security
%define debug_package %{nil}
%undefine __brp_mangle_shebangs

%description
strongswan

%prep
%setup -q -n strongswan-master

%build
./autogen.sh
%configure --enable-aesni --enable-openssl --enable-systemd --enable-eap-mschapv2 --enable-eap-identity --enable-md4 --with-systemdsystemunitdir=/usr/lib/systemd/system

make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
/etc/ipsec.conf
/etc/ipsec.secrets
/etc/strongswan.conf
/etc/strongswan.d
/etc/swanctl
/usr/bin/pki
/usr/lib/systemd/system/strongswan-starter.service
/usr/lib64/ipsec
/usr/libexec/ipsec
/usr/sbin/ipsec
/usr/sbin/swanctl
/usr/share/man/man*/*
/usr/share/strongswan
/usr/lib/systemd/system/strongswan.service
/usr/sbin/charon-systemd

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
