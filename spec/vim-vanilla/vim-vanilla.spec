Name:           vim-vanilla
Version:        9.0.0133
Release:        1%{?dist}
Summary:        The text editor
License:        GPLv2 and MIT
URL:            http://github.com/vim
Source0:        https://github.com/vim/vim/archive/refs/tags/v%{version}.tar.gz
BuildRequires:  gcc ncurses-devel

%description
vim

%prep
%setup -q -n vim-%{version}

%build
./configure --enable-cscope
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
rm -rf %{buildroot}/usr/local/share/man
rm -rf %{buildroot}/usr/local/share/applications/vim.desktop
rm -rf %{buildroot}/usr/local/share/applications/gvim.desktop
rm -rf %{buildroot}/usr/local/share/icons/hicolor/48x48/apps/gvim.png
rm -rf %{buildroot}/usr/local/share/icons/locolor/16x16/apps/gvim.png
rm -rf %{buildroot}/usr/local/share/icons/locolor/32x32/apps/gvim.png
rm -rf %{buildroot}/usr/local/share/vim/vim*/tools/
# For debug headers
cp %{_builddir}/vim-*/src/xxd/xxd %{buildroot}/usr/local/bin/
cp %{_builddir}/vim-*/src/vim %{buildroot}/usr/local/bin/

%files
/usr/local/bin/vim
/usr/local/bin/ex
/usr/local/bin/rview
/usr/local/bin/rvim
/usr/local/bin/view
/usr/local/bin/vimdiff
/usr/local/bin/vimtutor
/usr/local/bin/xxd
/usr/local/share/vim/vim*

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
