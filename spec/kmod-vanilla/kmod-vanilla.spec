Name:           kmod-vanilla
Version:        29
Release:        102%{?dist}
Summary:        IPSec utility
License:        GPLv2 and MIT
URL:            http://github.com/kmod
Source0:        https://mirrors.edge.kernel.org/pub/linux/utils/kernel/kmod/kmod-%{version}.tar.xz
BuildRequires:  gcc openssl-devel libzstd-devel xz-devel zlib-devel python-devel
Conflicts:      kmod kmod-libs
Provides:       kmod kmod-libs
Obsoletes:      kmod kmod-libs

%description
kmod

%prep
%setup -q -n kmod-%{version}

%build
%configure --with-zlib --with-zstd --with-openssl --with-xz --enable-experimental --enable-largefile --enable-python --enable-tools
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
cd %{buildroot}/usr/bin
ln -s kmod modprobe
ln -s kmod depmod
ln -s kmod insmod
ln -s kmod lsmod
ln -s kmod rmmod
ln -s kmod modinfo
mkdir -p %{buildroot}/usr/sbin
cd %{buildroot}/usr/sbin
ln -s ../bin/kmod kmod
ln -s ../bin/kmod modprobe
ln -s ../bin/kmod depmod
ln -s ../bin/kmod insmod
ln -s ../bin/kmod lsmod
ln -s ../bin/kmod rmmod
ln -s ../bin/kmod modinfo

%files
/usr/bin/kmod
/usr/bin/depmod
/usr/bin/modprobe
/usr/bin/insmod
/usr/bin/lsmod
/usr/bin/rmmod
/usr/bin/modinfo
/usr/sbin/kmod
/usr/sbin/depmod
/usr/sbin/modprobe
/usr/sbin/insmod
/usr/sbin/lsmod
/usr/sbin/rmmod
/usr/sbin/modinfo
/usr/include/libkmod.h
/usr/lib64/libkmod*
/usr/lib64/pkgconfig/libkmod.pc
/usr/share/bash-completion/completions/kmod
/usr/share/man/man5/depmod.d.5.gz
/usr/share/man/man5/modprobe.d.5.gz
/usr/share/man/man5/modules.dep.5.gz
/usr/share/man/man5/modules.dep.bin.5.gz
/usr/share/man/man8/depmod.8.gz
/usr/share/man/man8/insmod.8.gz
/usr/share/man/man8/kmod.8.gz
/usr/share/man/man8/lsmod.8.gz
/usr/share/man/man8/modinfo.8.gz
/usr/share/man/man8/modprobe.8.gz
/usr/share/man/man8/rmmod.8.gz
/usr/lib64/python*/site-packages/kmod



%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
