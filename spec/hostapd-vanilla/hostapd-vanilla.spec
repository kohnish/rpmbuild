Name:           hostapd-vanilla
Version:        20.0
Release:        3%{?dist}
Summary:        IPSec utility
License:        GPLv2 and MIT
URL:            http://github.com/hostapd
Source0:        https://github.com/kohnish/hostapd/archive/refs/heads/master.zip
Source1:        hostapd.service
BuildRequires:  gcc libnl3-devel openssl-devel
Conflicts:      hostapd

%description
hostapd

%prep
%setup -q -n hostapd-master

%build
cd hostapd
cp defconfig .config
make BINDIR=/usr/sbin %{?_smp_mflags}
DESTDIR=%{buildroot} make BINDIR=/usr/sbin install
mkdir -p %{buildroot}/usr/lib/systemd/system
mkdir -p %{buildroot}/etc/sysconfig
touch %{buildroot}/etc/sysconfig/hostapd
cp %{SOURCE1} %{buildroot}/usr/lib/systemd/system/hostapd.service

%files
/usr/sbin/hostapd
/usr/sbin/hostapd_cli
/usr/lib/systemd/system/hostapd.service
/etc/sysconfig/hostapd



%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
