%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
Name:           typescript-language-server
Version:        0.2
Release:        0%{?dist}
Summary:        typescript-language-server
License:        GPLv2
URL:            https://gitlab.com/typescript-language-server
Source0:        https://github.com/theia-ide/typescript-language-server/archive/refs/heads/master.zip
Requires:       nodejs typescript-vanilla
BuildRequires:  npm yarnpkg typescript-vanilla jq

%description
typescript-language-server

%prep
%setup -q -n typescript-language-server-master

%build
yarn --ignore-scripts --frozen-lockfile
yarn compile
yarn remove --frozen-lockfile $(jq -r '.devDependencies | keys | join(" ")' package.json)

%install
mkdir -p %{buildroot}/opt/typescript-language-server
mkdir -p %{buildroot}/usr/bin/
cp -rp %{_builddir}/typescript-language-server-master/* %{buildroot}/opt/typescript-language-server/
cd %{buildroot}/usr/bin/
chmod +x ../../opt/typescript-language-server/lib/cli.js
ln -s ../../opt/typescript-language-server/lib/cli.js ./typescript-language-server

%files
/opt/typescript-language-server
/usr/bin/typescript-language-server

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

