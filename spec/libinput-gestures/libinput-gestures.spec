Name:           libinput-gestures
Version:        2.51
Release:        1%{?dist}
Summary:        A Gtk based drop down terminal for Linux and Unix

License:        GPLv2 and MIT
URL:            https://github.com/bulletmark/libinput-gestures
Source0:        https://github.com/bulletmark/libinput-gestures/archive/2.51.tar.gz

%description
libinput-gestures

%prep
%setup -q -n %{name}-%{version}

%install
make install DESTDIR=%{buildroot}

%files
/etc/libinput-gestures.conf
/usr/bin/libinput-gestures
/usr/bin/libinput-gestures-setup
/usr/share/applications/libinput-gestures.desktop
/usr/share/doc/libinput-gestures
/usr/share/doc/libinput-gestures/README.md
/usr/share/icons/hicolor/128x128/apps/libinput-gestures.svg

%changelog
* Mon May 04 2020 kohnish <kohnish@gmx.com> - 2.51
- Initial commit
