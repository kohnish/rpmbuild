%global debug_package %{nil}

Name:           serve-d
Version:        0.0.2
Release:        1%{?dist}
Summary:        serve-d
License:        GPLv2
Source0:        https://github.com/Pure-D/serve-d/archive/refs/heads/master.zip
Source1:        https://github.com/dlang-community/DCD/releases/download/v0.13.6/dcd-v0.13.6-linux-x86_64.tar.gz
BuildRequires:  dmd gcc

%description
serve-d

%prep
#%setup -q -n serve-d-%{version}
%setup -q -n serve-d-master

%build
dub build --build=release

%install
mkdir -p %{buildroot}/usr/bin
cp %{_builddir}/serve-d-master/serve-d %{buildroot}/usr/bin/
cd %{buildroot}/usr/bin
tar zvxf %{SOURCE1}

%files
/usr/bin/serve-d
/usr/bin/dcd-client
/usr/bin/dcd-server

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

