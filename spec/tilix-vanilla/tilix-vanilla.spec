%global debug_package %{nil}

Name:           tilix-vanilla
Version:        2.1.4
Release:        3%{?dist}
Summary:        tilix vanilla
License:        GPLv2
URL:            https://github.com/kohnish/tilix
#Source0:        https://github.com/gnunn1/tilix/archive/refs/heads/master.zip
Source0:        https://github.com/kohnish/tilix/archive/refs/heads/master.zip
BuildRequires:  dmd gcc libX11-devel glib2-devel gettext-devel desktop-file-utils gtk-update-icon-cache gdk-pixbuf2-devel
Requires:       vte291
Conflicts:      tilix

%description
tilix vanilla

%prep
#%setup -q -n tilix-%{version}
%setup -q -n tilix-master

%build
dub build --build=release
cp data/man/tilix.1 data/man/tilix

%install
%{_builddir}/tilix-master/install.sh %{buildroot}/usr
strip --strip-unneeded %{buildroot}/usr/bin/tilix

%files
/usr/bin/tilix
/usr/share/applications/com.gexperts.Tilix.desktop
/usr/share/dbus-1/services/com.gexperts.Tilix.service
/usr/share/glib-2.0/schemas/com.gexperts.Tilix.gschema.xml
/usr/share/glib-2.0/schemas/gschemas.compiled
/usr/share/icons/hicolor/scalable/apps/com.gexperts.Tilix-symbolic.svg
/usr/share/icons/hicolor/scalable/apps/com.gexperts.Tilix.svg
/usr/share/locale/ak/LC_MESSAGES/tilix.mo
/usr/share/locale/ar/LC_MESSAGES/tilix.mo
/usr/share/locale/ar_MA/LC_MESSAGES/tilix.mo
/usr/share/locale/bg/LC_MESSAGES/tilix.mo
/usr/share/locale/ca/LC_MESSAGES/tilix.mo
/usr/share/locale/cs/LC_MESSAGES/tilix.mo
/usr/share/locale/de/LC_MESSAGES/tilix.mo
/usr/share/locale/el/LC_MESSAGES/tilix.mo
/usr/share/locale/en_GB/LC_MESSAGES/tilix.mo
/usr/share/locale/eo/LC_MESSAGES/tilix.mo
/usr/share/locale/es/LC_MESSAGES/tilix.mo
/usr/share/locale/eu/LC_MESSAGES/tilix.mo
/usr/share/locale/fi/LC_MESSAGES/tilix.mo
/usr/share/locale/fr/LC_MESSAGES/tilix.mo
/usr/share/locale/gl/LC_MESSAGES/tilix.mo
/usr/share/locale/he/LC_MESSAGES/tilix.mo
/usr/share/locale/hr/LC_MESSAGES/tilix.mo
/usr/share/locale/hu/LC_MESSAGES/tilix.mo
/usr/share/locale/id/LC_MESSAGES/tilix.mo
/usr/share/locale/it/LC_MESSAGES/tilix.mo
/usr/share/locale/ja/LC_MESSAGES/tilix.mo
/usr/share/locale/ko/LC_MESSAGES/tilix.mo
/usr/share/locale/lt/LC_MESSAGES/tilix.mo
/usr/share/locale/mr/LC_MESSAGES/tilix.mo
/usr/share/locale/nb_NO/LC_MESSAGES/tilix.mo
/usr/share/locale/nl/LC_MESSAGES/tilix.mo
/usr/share/locale/oc/LC_MESSAGES/tilix.mo
/usr/share/locale/pl/LC_MESSAGES/tilix.mo
/usr/share/locale/pt_BR/LC_MESSAGES/tilix.mo
/usr/share/locale/pt_PT/LC_MESSAGES/tilix.mo
/usr/share/locale/ro/LC_MESSAGES/tilix.mo
/usr/share/locale/ru/LC_MESSAGES/tilix.mo
/usr/share/locale/sr/LC_MESSAGES/tilix.mo
/usr/share/locale/sv/LC_MESSAGES/tilix.mo
/usr/share/locale/tr/LC_MESSAGES/tilix.mo
/usr/share/locale/uk/LC_MESSAGES/tilix.mo
/usr/share/locale/vi/LC_MESSAGES/tilix.mo
/usr/share/locale/zh_CN/LC_MESSAGES/tilix.mo
/usr/share/locale/zh_TW/LC_MESSAGES/tilix.mo
/usr/share/man/man1/tilix.1.gz
/usr/share/metainfo/com.gexperts.Tilix.appdata.xml
/usr/share/nautilus-python/extensions/open-tilix.py
/usr/share/tilix/resources/tilix.gresource
/usr/share/tilix/schemes/base16-twilight-dark.json
/usr/share/tilix/schemes/linux.json
/usr/share/tilix/schemes/material.json
/usr/share/tilix/schemes/monokai.json
/usr/share/tilix/schemes/orchis.json
/usr/share/tilix/schemes/solarized-dark.json
/usr/share/tilix/schemes/solarized-light.json
/usr/share/tilix/schemes/tango.json
/usr/share/tilix/schemes/yaru.json
/usr/share/tilix/scripts/install-man-pages.sh
/usr/share/tilix/scripts/tilix_int.sh

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

