%global __brp_check_rpaths %{nil}
%global optflags %{optflags} -Wno-format-security
%define debug_package %{nil}
%undefine __brp_mangle_shebangs

Name:           openssl-vanilla
Version:        3.3.2
Release:        0%{?dist}
Summary:        OpenSSL
License:        GPLv2 and MIT
URL:            http://github.com/openssl
Source0:        https://github.com/openssl/openssl/releases/download/openssl-%{version}/openssl-%{version}.tar.gz
BuildRequires:  gcc
BuildRequires:  gettext-devel
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  libevent
BuildRequires:  m4
BuildRequires:  gperf
BuildRequires:  bison
BuildRequires:  flex
BuildRequires:  gmp-devel
BuildRequires:  perl-IPC-Cmd
BuildRequires:  perl-FindBin
BuildRequires: perl-lib

%description
openssl

%prep
%setup -q -n openssl-%{version}

%build
./config -Wl,-rpath=/opt/vanilla/lib -Wl,--enable-new-dtags --libdir=lib --prefix=/opt/vanilla --openssldir=/opt/vanilla shared no-asm enable-ssl2 enable-ssl3 enable-tls -fPIC -DOPENSSL_PIC enable-weak-ssl-ciphers no-tests
make %{?_smp_mflags}

%install
make install_sw DESTDIR=%{buildroot}

%files
/opt/vanilla/*

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
