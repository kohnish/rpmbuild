Name:           rofi-vanilla
Version:        1.5.4
Release:        1%{?dist}
Summary:        Rofi
License:        GPLv2 and MIT
URL:            http://github.com/rofi
Source0:        https://github.com/davatorium/rofi/releases/download/%{version}/rofi-%{version}.tar.xz
BuildRequires:  gcc ncurses-devel libICE-devel libSM-devel libX11-devel libXt-devel make autoconf automake pkg-config flex bison pango-devel cairo-devel librsvg2-devel glib2-devel startup-notification-devel libxcb-devel libxkbcommon-devel xcb-util-devel xcb-util-image-devel xcb-util-renderutil-devel xcb-util-cursor-devel xcb-util-xrm-devel xcb-util-keysyms-devel xcb-util-wm-devel libxkbcommon-x11-devel



%description
rofi

%prep
%setup -q -n rofi-%{version}

%build
./configure --disable-check

%install
make install DESTDIR=%{buildroot}

%files
/usr/local/bin/rofi
/usr/local/bin/rofi-sensible-terminal
/usr/local/bin/rofi-theme-selector
/usr/local/include/rofi/helper.h
/usr/local/include/rofi/mode-private.h
/usr/local/include/rofi/mode.h
/usr/local/include/rofi/rofi-icon-fetcher.h
/usr/local/include/rofi/rofi-types.h
/usr/local/lib/pkgconfig/rofi.pc
/usr/local/share/man/man1/rofi-sensible-terminal.1
/usr/local/share/man/man1/rofi-theme-selector.1
/usr/local/share/man/man1/rofi.1
/usr/local/share/man/man5/rofi-theme.5
/usr/local/share/rofi/themes/Adapta-Nokto.rasi
/usr/local/share/rofi/themes/Arc-Dark.rasi
/usr/local/share/rofi/themes/Arc.rasi
/usr/local/share/rofi/themes/DarkBlue.rasi
/usr/local/share/rofi/themes/Indego.rasi
/usr/local/share/rofi/themes/Monokai.rasi
/usr/local/share/rofi/themes/Paper.rasi
/usr/local/share/rofi/themes/Pop-Dark.rasi
/usr/local/share/rofi/themes/android_notification.rasi
/usr/local/share/rofi/themes/arthur.rasi
/usr/local/share/rofi/themes/blue.rasi
/usr/local/share/rofi/themes/c64.rasi
/usr/local/share/rofi/themes/dmenu.rasi
/usr/local/share/rofi/themes/fancy.rasi
/usr/local/share/rofi/themes/glue_pro_blue.rasi
/usr/local/share/rofi/themes/gruvbox-common.rasi
/usr/local/share/rofi/themes/gruvbox-dark-hard.rasi
/usr/local/share/rofi/themes/gruvbox-dark-soft.rasi
/usr/local/share/rofi/themes/gruvbox-dark.rasi
/usr/local/share/rofi/themes/gruvbox-light-hard.rasi
/usr/local/share/rofi/themes/gruvbox-light-soft.rasi
/usr/local/share/rofi/themes/gruvbox-light.rasi
/usr/local/share/rofi/themes/lb.rasi
/usr/local/share/rofi/themes/paper-float.rasi
/usr/local/share/rofi/themes/purple.rasi
/usr/local/share/rofi/themes/sidebar.rasi
/usr/local/share/rofi/themes/solarized.rasi
/usr/local/share/rofi/themes/solarized_alternate.rasi

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1
- Initial
