%global optflags %{optflags} -Wno-format-security -Os -fno-lto
%global _hardened_build 0
%define _lto_cflags %{nil}
%define usbmuxd_commit 61b99ab5c25609c11369733a0df97c03a0581a56
%define libimobiledevice_commit 27d5940e17b27ee59e5deaf68a92bb006042bdc5
%define libimobiledevice_version 1.3.0

Name:           usbmuxd-vanilla
Version:        1.1.1
Release:        5%{?dist}
Summary:        usbmuxd
License:        GPLv2 and MIT
URL:            http://github.com/usbmuxd
Source0:        https://github.com/libimobiledevice/usbmuxd/archive/%{usbmuxd_commit}.zip
Source1:        https://github.com/kohnish/libimobiledevice/archive/%{libimobiledevice_commit}.zip
BuildRequires:  gcc
BuildRequires:  gettext-devel
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  libevent
BuildRequires:  m4
BuildRequires:  gperf
BuildRequires:  bison
BuildRequires:  flex
BuildRequires:  gmp-devel
BuildRequires:  git
BuildRequires:  systemd-devel
BuildRequires:  libusb1-devel libplist-devel libimobiledevice-glue-devel libusbmuxd-devel
BuildRequires:  perl-IPC-Cmd
BuildRequires:  perl-FindBin
BuildRequires:  perl-lib
BuildRequires:  unzip
BuildRequires:  openssl-devel
BuildRequires:  libgcrypt-devel
Conflicts: usbmuxd

%description
usbmuxd

%prep
%setup -q -n usbmuxd-%{usbmuxd_commit}

%build
unzip %{SOURCE1}
cd libimobiledevice-%{libimobiledevice_commit}
echo %{libimobiledevice_version} > .tarball-version
PKG_CONFIG_PATH=$pkgconfig_dir ./autogen.sh --prefix=`pwd`/install --disable-shared --with-openssl
make %{?_smp_mflags}
make install
pkgconfig_dir=$pkgconfig_dir:`pwd`/install/lib/pkgconfig

cd ..

echo %{version} > .tarball-version
PKG_CONFIG_PATH="$pkgconfig_dir" ./autogen.sh --prefix=/usr --with-gnutls --with-systemd --with-systemdsystemunitdir=/usr/lib/systemd/system --with-udevrulesdir=/usr/lib/udev/rules.d
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
/usr/sbin/usbmuxd
/usr/share/man/man8/usbmuxd.8.gz
/usr/lib/systemd/system/usbmuxd.service
/usr/lib/udev/rules.d/39-usbmuxd.rules

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
