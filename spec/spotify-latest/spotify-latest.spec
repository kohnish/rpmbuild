%global debug_package %{nil}
AutoReqProv: no

Name:           spotify-latest
Version:        1.2.26.1187
Release:        1%{?dist}
Summary:        Spotify
License:        GPLv2
URL:            https://gitlab.com/kohnish/spotify
BuildRequires:  p7zip p7zip-plugins
Requires: curl libatomic libappindicator-gtk3

%description
spotify

%prep

%build
curl -L -o spotify.deb http://repository.spotify.com/pool/non-free/s/spotify-client/spotify-client_%{version}.g36b715a1_amd64.deb
7z x -aoa spotify.deb

%install
tar --overwrite -zxf data.tar.gz -C%{buildroot}
mkdir -p %{buildroot}/usr/lib64
cd %{buildroot}/usr/lib64
touch libcurl.so.4
ln -sf libcurl.so.4 libcurl-gnutls.so.4
touch libappindicator3.so.1.0.0
ln -sf libappindicator3.so.1.0.0 libayatana-appindicator3.so.1
mkdir -p %{buildroot}/usr/share/applications
cd %{buildroot}/usr/share/applications
ln -s ../spotify/spotify.desktop spotify.desktop


%files
%exclude /usr/lib64/libcurl.so.4
%exclude /usr/lib64/libappindicator3.so.1.0.0
/usr/lib64/libcurl-gnutls.so.4
/usr/lib64/libayatana-appindicator3.so.1
/usr/share/applications/spotify.desktop
/usr/bin/spotify
/usr/share/doc/spotify-client/changelog.gz
/usr/share/spotify

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial
