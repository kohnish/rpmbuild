%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
Name:           typescript-vanilla
Version:        1.0
Release:        0%{?dist}
Summary:        typescript
License:        GPLv2
URL:            https://gitlab.com/typescript
Source0:        https://codeload.github.com/microsoft/TypeScript/zip/refs/heads/main
BuildRequires:  npm
Conflicts:  nodejs-typescript

%description
typescript

%prep
%setup -q -n TypeScript-main

%build
npm config set prefix '~/.local/npm'
export PATH=$PATH:~/.local/npm/bin
npm install -g gulp
npm ci
~/.local/npm/bin/gulp local

%install
mkdir -p %{buildroot}/opt/typescript
mkdir -p %{buildroot}/usr/bin/
cp -rp %{_builddir}/TypeScript-main/bin %{buildroot}/opt/typescript/
cp -rp %{_builddir}/TypeScript-main/lib %{buildroot}/opt/typescript/
cd %{buildroot}/usr/bin/
ln -s ../../opt/typescript/bin/tsserver ./
ln -s ../../opt/typescript/bin/tsc ./
chmod +x ../../opt/typescript/bin/tsserver
chmod +x ../../opt/typescript/bin/tsc

%files
/opt/typescript
/usr/bin/tsserver
/usr/bin/tsc

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

