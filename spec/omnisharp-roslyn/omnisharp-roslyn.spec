%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
%global __os_install_post %{nil}

Name:    omnisharp-roslyn
Version: 1.37.8
Release: 1
Group:   Development
License: GPLv2
Summary: %{name}
URL: https://github.com/kohnish/android-ndk
Source0: https://github.com/OmniSharp/omnisharp-roslyn/releases/download/v%{version}/omnisharp-linux-x64.tar.gz
Source1: csharp-ls

%description
c sharp lang server

%install
mkdir -p %{buildroot}/opt/omnisharp-roslyn
mkdir -p %{buildroot}/usr/bin
tar zxf %{SOURCE0} -C %{buildroot}/opt/omnisharp-roslyn
cp -p %{SOURCE1} %{buildroot}/opt/omnisharp-roslyn
cd %{buildroot}/usr/bin/
ln -s ../../opt/omnisharp-roslyn/csharp-ls ./

%files
/opt/omnisharp-roslyn
/usr/bin/csharp-ls

%changelog
* Tue Dec 10 2019 kohnish <kohnish@gmx.com> - 1.0
- Initial package
