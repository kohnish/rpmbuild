# -*-Mode: rpm-spec -*-

Name:     wayvnc-vanilla
Version:  1.0
Release:  3%{?dist}
Summary:  A VNC server for wlroots based Wayland compositors
License:  ISC
URL:      https://github.com/any1/wayvnc
Source0:  https://github.com/any1/wayvnc/archive/refs/heads/master.zip
BuildRequires: gcc
BuildRequires: meson
BuildRequires: git
BuildRequires: pam-devel
BuildRequires: scdoc
BuildRequires: pkgconfig(egl)
BuildRequires: pkgconfig(glesv2)
BuildRequires: pkgconfig(gnutls)
BuildRequires: pkgconfig(libdrm)
BuildRequires: pkgconfig(pixman-1)
BuildRequires: pkgconfig(wayland-client)
BuildRequires: pkgconfig(xkbcommon)
#BuildRequires: pkgconfig(neatvnc)
#BuildRequires: pkgconfig(aml)

%description

This is a VNC server for wlroots based Wayland compositors. It
attaches to a running Wayland session, creates virtual input devices
and exposes a single display via the RFB protocol. The Wayland session
may be a headless one, so it is also possible to run wayvnc without a
physical display attached.

%prep
%setup -n wayvnc-master

%build
mkdir -p subprojects
cd subprojects
git clone https://github.com/any1/neatvnc.git --depth=1
git clone https://github.com/any1/aml.git --depth=1
cd ..

meson setup build -Ddefault_library=static 
meson build --prefix=/usr -Ddefault_library=static
cd build
DESTDIR=%{buildroot} meson install

%files
/usr/local/bin/wayvnc
%exclude /usr/local/share/man/man1/wayvnc.1
%exclude /usr/local/include/neatvnc.h
%exclude /usr/local/lib64/libneatvnc.a
%exclude /usr/local/lib64/pkgconfig/neatvnc.pc
#/usr/include/aml.h
#/usr/include/neatvnc.h
#/usr/lib64/libaml.so
#/usr/lib64/libaml.so.0
#/usr/lib64/libaml.so.0.0.0
#/usr/lib64/libneatvnc.so
#/usr/lib64/libneatvnc.so.0
#/usr/lib64/libneatvnc.so.0.0.0
#/usr/lib64/pkgconfig/aml.pc
#/usr/lib64/pkgconfig/neatvnc.pc
#/usr/local/bin/wayvnc
#/usr/local/include/aml.h
#/usr/local/include/neatvnc.h
#/usr/local/lib64/libaml.so
#/usr/local/lib64/libaml.so.0
#/usr/local/lib64/libaml.so.0.0.0
#/usr/local/lib64/libneatvnc.so
#/usr/local/lib64/libneatvnc.so.0
#/usr/local/lib64/libneatvnc.so.0.0.0
#/usr/local/lib64/pkgconfig/aml.pc
#/usr/local/lib64/pkgconfig/neatvnc.pc
#/usr/local/share/man/man1/wayvnc.1
#/usr/share/man/man1/wayvnc.1
#/usr/bin/wayvnc

%changelog
* Sat Jul 23 2022 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild
