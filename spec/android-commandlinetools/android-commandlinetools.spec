%global debug_package %{nil}
AutoReqProv: no
Name:    android-commandlinetools
Version: 6609375
Release: 1
Group:   Development
License: GPLv2
Summary: %{name} sdkmanager
Source0: https://dl.google.com/android/repository/commandlinetools-linux-%{version}_latest.zip
URL: https://github.com/kohnish/android-commandlinetools

%description
sdkmanager

%prep
%setup -q -n tools

%build

%install
mkdir -p %{buildroot}/opt/android/
cp -r %{_builddir}/tools %{buildroot}/opt/android

%files
/opt/android/tools

%changelog
* Tue Dec 10 2019 kohnish <kohnish@gmx.com> - 1.0
- Initial package
