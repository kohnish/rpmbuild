%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
%global __os_install_post %{nil}
Name:    crd
Version: 0.0.1
Release: 1
Group:   Development
License: GPLv2
Summary: %{name}
URL: https://github.com/kohnish/crd
Source0: https://dl.google.com/linux/direct/chrome-remote-desktop_current_amd64.deb
BuildRequires: p7zip p7zip-plugins tar

%description
crd

%install
7z x ../SOURCES/chrome-remote-desktop_current_amd64.deb
tar fxv data.tar
rm -rf data.tar
mv * %{buildroot}/
cat > %{buildroot}/etc/pam.d/chrome-remote-desktop<<"EOF"
# Copyright (c) 2012 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

auth        required    pam_unix.so
account     required    pam_unix.so
password    required    pam_unix.so
session     required    pam_unix.so

session [success=ok ignore=ignore module_unknown=ignore default=bad] pam_selinux.so close
session required pam_limits.so

session [success=ok ignore=ignore module_unknown=ignore default=bad] pam_selinux.so open
session required pam_env.so readenv=1
session required pam_env.so readenv=1 user_readenv=1 envfile=/etc/default/locale
EOF


%files
/etc/cron.daily/chrome-remote-desktop
/etc/init.d/chrome-remote-desktop
/etc/opt/chrome/native-messaging-hosts/com.google.chrome.remote_assistance.json
/etc/opt/chrome/native-messaging-hosts/com.google.chrome.remote_desktop.json
/etc/pam.d/chrome-remote-desktop
/lib/systemd/system/chrome-remote-desktop.service
/lib/systemd/system/chrome-remote-desktop@.service
/opt/google/chrome-remote-desktop/Xsession
/opt/google/chrome-remote-desktop/chrome-remote-desktop
/opt/google/chrome-remote-desktop/chrome-remote-desktop-host
/opt/google/chrome-remote-desktop/configure-url-forwarder
/opt/google/chrome-remote-desktop/icudtl.dat
/opt/google/chrome-remote-desktop/is-remoting-session
/opt/google/chrome-remote-desktop/native-messaging-host
/opt/google/chrome-remote-desktop/remote-assistance-host
/opt/google/chrome-remote-desktop/remote-open-url
/opt/google/chrome-remote-desktop/remoting_locales/am.pak
/opt/google/chrome-remote-desktop/remoting_locales/am.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/ar.pak
/opt/google/chrome-remote-desktop/remoting_locales/ar.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/bg.pak
/opt/google/chrome-remote-desktop/remoting_locales/bg.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/bn.pak
/opt/google/chrome-remote-desktop/remoting_locales/bn.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/ca.pak
/opt/google/chrome-remote-desktop/remoting_locales/ca.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/cs.pak
/opt/google/chrome-remote-desktop/remoting_locales/cs.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/da.pak
/opt/google/chrome-remote-desktop/remoting_locales/da.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/de.pak
/opt/google/chrome-remote-desktop/remoting_locales/de.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/el.pak
/opt/google/chrome-remote-desktop/remoting_locales/el.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/en-GB.pak
/opt/google/chrome-remote-desktop/remoting_locales/en-GB.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/en.pak
/opt/google/chrome-remote-desktop/remoting_locales/en.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/es-419.pak
/opt/google/chrome-remote-desktop/remoting_locales/es-419.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/es.pak
/opt/google/chrome-remote-desktop/remoting_locales/es.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/et.pak
/opt/google/chrome-remote-desktop/remoting_locales/et.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/fa.pak
/opt/google/chrome-remote-desktop/remoting_locales/fa.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/fi.pak
/opt/google/chrome-remote-desktop/remoting_locales/fi.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/fil.pak
/opt/google/chrome-remote-desktop/remoting_locales/fil.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/fr.pak
/opt/google/chrome-remote-desktop/remoting_locales/fr.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/gu.pak
/opt/google/chrome-remote-desktop/remoting_locales/gu.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/he.pak
/opt/google/chrome-remote-desktop/remoting_locales/he.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/hi.pak
/opt/google/chrome-remote-desktop/remoting_locales/hi.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/hr.pak
/opt/google/chrome-remote-desktop/remoting_locales/hr.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/hu.pak
/opt/google/chrome-remote-desktop/remoting_locales/hu.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/id.pak
/opt/google/chrome-remote-desktop/remoting_locales/id.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/it.pak
/opt/google/chrome-remote-desktop/remoting_locales/it.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/ja.pak
/opt/google/chrome-remote-desktop/remoting_locales/ja.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/kn.pak
/opt/google/chrome-remote-desktop/remoting_locales/kn.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/ko.pak
/opt/google/chrome-remote-desktop/remoting_locales/ko.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/lt.pak
/opt/google/chrome-remote-desktop/remoting_locales/lt.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/lv.pak
/opt/google/chrome-remote-desktop/remoting_locales/lv.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/ml.pak
/opt/google/chrome-remote-desktop/remoting_locales/ml.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/mr.pak
/opt/google/chrome-remote-desktop/remoting_locales/mr.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/ms.pak
/opt/google/chrome-remote-desktop/remoting_locales/ms.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/nb.pak
/opt/google/chrome-remote-desktop/remoting_locales/nb.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/nl.pak
/opt/google/chrome-remote-desktop/remoting_locales/nl.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/pl.pak
/opt/google/chrome-remote-desktop/remoting_locales/pl.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/pt-BR.pak
/opt/google/chrome-remote-desktop/remoting_locales/pt-BR.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/pt-PT.pak
/opt/google/chrome-remote-desktop/remoting_locales/pt-PT.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/ro.pak
/opt/google/chrome-remote-desktop/remoting_locales/ro.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/ru.pak
/opt/google/chrome-remote-desktop/remoting_locales/ru.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/sk.pak
/opt/google/chrome-remote-desktop/remoting_locales/sk.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/sl.pak
/opt/google/chrome-remote-desktop/remoting_locales/sl.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/sr.pak
/opt/google/chrome-remote-desktop/remoting_locales/sr.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/sv.pak
/opt/google/chrome-remote-desktop/remoting_locales/sv.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/sw.pak
/opt/google/chrome-remote-desktop/remoting_locales/sw.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/ta.pak
/opt/google/chrome-remote-desktop/remoting_locales/ta.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/te.pak
/opt/google/chrome-remote-desktop/remoting_locales/te.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/th.pak
/opt/google/chrome-remote-desktop/remoting_locales/th.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/tr.pak
/opt/google/chrome-remote-desktop/remoting_locales/tr.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/uk.pak
/opt/google/chrome-remote-desktop/remoting_locales/uk.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/vi.pak
/opt/google/chrome-remote-desktop/remoting_locales/vi.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/zh-CN.pak
/opt/google/chrome-remote-desktop/remoting_locales/zh-CN.pak.info
/opt/google/chrome-remote-desktop/remoting_locales/zh-TW.pak
/opt/google/chrome-remote-desktop/remoting_locales/zh-TW.pak.info
/opt/google/chrome-remote-desktop/start-host
/opt/google/chrome-remote-desktop/user-session
/usr/lib/mozilla/native-messaging-hosts/com.google.chrome.remote_assistance.json
/usr/lib/mozilla/native-messaging-hosts/com.google.chrome.remote_desktop.json
/usr/share/applications/crd-url-forwarder.desktop
/usr/share/doc/chrome-remote-desktop/CREDITS.txt.gz
/usr/share/doc/chrome-remote-desktop/changelog.gz
/usr/share/doc/chrome-remote-desktop/copyright

%changelog
* Tue Dec 10 2019 kohnish <kohnish@gmx.com> - 1.0
- Initial package
