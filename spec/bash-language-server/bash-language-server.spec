%global debug_package %{nil}
%define __strip /bin/true
AutoReqProv: no
Name:           bash-language-server
Version:        0.1
Release:        1%{?dist}
Summary:        bash-language-server
License:        GPLv2
URL:            https://gitlab.com/bash-language-server
Source0:        https://github.com/bash-lsp/bash-language-server/archive/refs/heads/master.zip
BuildRequires:  npm yarnpkg nodejs-typescript

%description
bash-language-server

%prep
%setup -q -n bash-language-server-master

%build
yarn
yarn run compile

%install
mkdir -p %{buildroot}/opt/bash-language-server
mkdir -p %{buildroot}/usr/bin/
cp -rp %{_builddir}/bash-language-server-master/server/* %{buildroot}/opt/bash-language-server/
cd %{buildroot}/usr/bin/
ln -s ../../opt/bash-language-server/bin/main.js bash-language-server

%files
/opt/bash-language-server
/usr/bin/bash-language-server

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

