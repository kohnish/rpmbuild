%global debug_package %{nil}
AutoReqProv: no

Name:           dmd
Version:        2.096
Release:        1%{?dist}
Summary:        dlang
License:        GPLv2
URL:            https://gitlab.com/kohnish/d
BuildRequires:  curl

%description
dmd

%prep

%build
curl -L -o dmd.rpm https://s3.us-west-2.amazonaws.com/downloads.dlang.org/releases/2021/dmd-2.096.1-0.fedora.x86_64.rpm

%install
cd %{buildroot}
rpm2cpio %{_builddir}/dmd.rpm | cpio -idmv

%files
/etc/bash_completion.d/dmd
/etc/dmd.conf
/usr/bin/ddemangle
/usr/bin/dmd
/usr/bin/dub
/usr/bin/dustmite
/usr/bin/rdmd
/usr/include/dmd
/usr/share/dmd
/usr/lib/libphobos2.a
/usr/lib/libphobos2.so
/usr/lib/libphobos2.so.0.96
/usr/lib/libphobos2.so.0.96.1
/usr/lib64/libphobos2.a
/usr/lib64/libphobos2.so
/usr/lib64/libphobos2.so.0.96
/usr/lib64/libphobos2.so.0.96.1
/usr/share/doc/dmd/copyright
/usr/share/man/man1/dmd.1.gz
/usr/share/man/man1/rdmd.1.gz
/usr/share/man/man5/dmd.conf.5.gz


%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

