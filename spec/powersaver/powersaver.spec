Name:           powersaver
Version:        1.0
Release:        0%{?dist}
Summary:        Save power by limiting CPU power
License:        GPLv2
URL:            https://gitlab.com/kohnish/powersaver
Source0:        https://gitlab.com/kohnish/powersaver/-/archive/%{version}/powersaver-%{version}.tar.bz2
BuildRequires:  gcc gcc-g++ make cmake libyaml-devel

%description
Limit CPU based on specified load averages.

%prep
%setup -q -n powersaver-%{version}

%build
cmake -DBUILD_TEST=OFF -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/usr .
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
/usr/lib/systemd/system/powersaver.service
/usr/bin/powersaver
%config /etc/powersaver/config.yaml

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

