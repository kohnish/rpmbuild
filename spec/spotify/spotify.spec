%global debug_package %{nil}
AutoReqProv: no

Name:           spotify
Version:        1.0.80
Release:        2%{?dist}
Summary:        Spotify
License:        GPLv2
URL:            https://gitlab.com/kohnish/spotify
BuildRequires:  p7zip p7zip-plugins
Requires: curl libatomic

%description
spotify

%prep

%build
curl -L -o spotify.deb http://th.archive.ubuntu.com/linuxmint-packages/pool/import/s/spotify-client/spotify-client_1.0.80.480.g51b03ac3-13_amd64.deb
7z x -aoa spotify.deb


%install
tar --overwrite -zxf data.tar.gz -C%{buildroot}
mkdir -p %{buildroot}/usr/lib64
cd %{buildroot}/usr/lib64
touch libcurl.so.4
ln -sf libcurl.so.4 libcurl-gnutls.so.4
mkdir -p %{buildroot}/usr/share/applications
cd %{buildroot}/usr/share/applications
ln -s ../spotify/spotify.desktop spotify.desktop


%files
%exclude /usr/lib64/libcurl.so.4
/usr/lib64/libcurl-gnutls.so.4
/usr/share/applications/spotify.desktop
/usr/bin/spotify
/usr/share/doc/spotify-client/changelog.gz
/usr/share/spotify/Apps/about.spa
/usr/share/spotify/Apps/artist.spa
/usr/share/spotify/Apps/browse.spa
/usr/share/spotify/Apps/buddy-list.spa
/usr/share/spotify/Apps/chart.spa
/usr/share/spotify/Apps/collection-album.spa
/usr/share/spotify/Apps/collection-artist.spa
/usr/share/spotify/Apps/collection-songs.spa
/usr/share/spotify/Apps/collection.spa
/usr/share/spotify/Apps/concert.spa
/usr/share/spotify/Apps/concerts.spa
/usr/share/spotify/Apps/daily-mix-hub.spa
/usr/share/spotify/Apps/error.spa
/usr/share/spotify/Apps/findfriends.spa
/usr/share/spotify/Apps/full-screen-modal.spa
/usr/share/spotify/Apps/genre.spa
/usr/share/spotify/Apps/glue-resources.spa
/usr/share/spotify/Apps/hub.spa
/usr/share/spotify/Apps/licenses.spa
/usr/share/spotify/Apps/login.spa
/usr/share/spotify/Apps/lyrics.spa
/usr/share/spotify/Apps/playlist-folder.spa
/usr/share/spotify/Apps/playlist.spa
/usr/share/spotify/Apps/profile.spa
/usr/share/spotify/Apps/queue.spa
/usr/share/spotify/Apps/radio-hub.spa
/usr/share/spotify/Apps/search.spa
/usr/share/spotify/Apps/settings.spa
/usr/share/spotify/Apps/show.spa
/usr/share/spotify/Apps/station.spa
/usr/share/spotify/Apps/stations.spa
/usr/share/spotify/Apps/zlink.spa
/usr/share/spotify/apt-keys/spotify-2017-07-25-341D9410.gpg
/usr/share/spotify/apt-keys/spotify-2018-05-23-48BF1C90.gpg
/usr/share/spotify/cef.pak
/usr/share/spotify/cef_100_percent.pak
/usr/share/spotify/cef_200_percent.pak
/usr/share/spotify/cef_extensions.pak
/usr/share/spotify/devtools_resources.pak
/usr/share/spotify/icons/spotify-linux-128.png
/usr/share/spotify/icons/spotify-linux-16.png
/usr/share/spotify/icons/spotify-linux-22.png
/usr/share/spotify/icons/spotify-linux-24.png
/usr/share/spotify/icons/spotify-linux-256.png
/usr/share/spotify/icons/spotify-linux-32.png
/usr/share/spotify/icons/spotify-linux-48.png
/usr/share/spotify/icons/spotify-linux-512.png
/usr/share/spotify/icons/spotify-linux-64.png
/usr/share/spotify/icons/spotify_icon.ico
/usr/share/spotify/icudtl.dat
/usr/share/spotify/libEGL.so
/usr/share/spotify/libGLESv2.so
/usr/share/spotify/libcef.so
/usr/share/spotify/libwidevinecdmadapter.so
/usr/share/spotify/locales/arb.mo
/usr/share/spotify/locales/cs.mo
/usr/share/spotify/locales/de.mo
/usr/share/spotify/locales/el.mo
/usr/share/spotify/locales/en-US.pak
/usr/share/spotify/locales/en.mo
/usr/share/spotify/locales/es-419.mo
/usr/share/spotify/locales/es.mo
/usr/share/spotify/locales/fi.mo
/usr/share/spotify/locales/fr-CA.mo
/usr/share/spotify/locales/fr.mo
/usr/share/spotify/locales/hu.mo
/usr/share/spotify/locales/id.mo
/usr/share/spotify/locales/it.mo
/usr/share/spotify/locales/ja.mo
/usr/share/spotify/locales/nl.mo
/usr/share/spotify/locales/pl.mo
/usr/share/spotify/locales/pt-BR.mo
/usr/share/spotify/locales/sv.mo
/usr/share/spotify/locales/th.mo
/usr/share/spotify/locales/tr.mo
/usr/share/spotify/locales/vi.mo
/usr/share/spotify/locales/zh-Hant.mo
/usr/share/spotify/locales/zsm.mo
/usr/share/spotify/natives_blob.bin
/usr/share/spotify/snapshot_blob.bin
/usr/share/spotify/spotify
/usr/share/spotify/spotify.desktop
/usr/share/spotify/swiftshader/libEGL.so
/usr/share/spotify/swiftshader/libGLESv2.so
/usr/share/spotify/v8_context_snapshot.bin

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

